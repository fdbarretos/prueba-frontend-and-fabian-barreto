# PruebaFrontEndAND

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 8.3.17.

Este proyecto se desarrolla para realizar la prueba de conocimiento tecnico para el cargo desarrollador angular en la Agencia Nacional Digital

## Server de desarrollo

Primero realice el clon del proyecto desde consola ejecute `git clone git@gitlab.com:fdbarretos/prueba-frontend-and-fabian-barreto.git`, despues ejecute el comando `npm i` para instalar las dependecias necesarias.


Ejecute `ng serve`  para un servidor de desarrollo. Navegar a `http://localhost:4200/`. La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.

## Build

Ejecute `ng build` para construir el proyecto. Los artefactos de construcción se almacenarán en el  directorio `dist/`. Utilizar la bandera `--prod`  para una construcción de producción.

## Running unit tests

Ejecute `ng test` para ejecutar las pruebas unitarias a través de [Karma] (https://karma-runner.github.io).

## Running end-to-end tests

Ejecute `ng e2e` para ejecutar las pruebas de extremo a extremo a través de [Protractor] (http://www.protractortest.org/).
