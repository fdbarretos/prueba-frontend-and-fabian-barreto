import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConoceropinionComponent } from './conoceropinion.component';

describe('ConoceropinionComponent', () => {
  let component: ConoceropinionComponent;
  let fixture: ComponentFixture<ConoceropinionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConoceropinionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConoceropinionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
