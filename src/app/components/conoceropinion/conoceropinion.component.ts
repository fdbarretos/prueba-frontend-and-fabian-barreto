import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conoceropinion',
  templateUrl: './conoceropinion.component.html',
  styleUrls: ['./conoceropinion.component.css']
})
export class ConoceropinionComponent implements OnInit {
  contador: any;
  maximocont = 255;
  checkmax: any;
  constructor() { }

  // Funcion para llevar control al textarea
  onKey(event) {
    this.maximocont = 255;

    this.contador = event.target.value;
    this.maximocont = this.maximocont - this.contador.length;
    if (this.maximocont === 0) {
      this.checkmax = 'Máximo caracteres alcanzado!';
    } else {
      this.checkmax = '';
    }
   }


  ngOnInit() {
  }

}
